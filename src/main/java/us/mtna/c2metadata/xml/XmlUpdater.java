package us.mtna.c2metadata.xml;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;

import us.mtna.pojo.DataSet;

/**
 * An XML updater will produce a new XML document given the original source XML
 * document and the data sets that result from processing a set of transforms
 * performed on the data set(s) in the original XML document.
 * 
 * @author Jack Gager (j.gager@mtna.us)
 *
 */
public interface XmlUpdater {

	/**
	 * Creates a new XML document, which is an update to the original XML
	 * document, accounting for the data sets that are derived from a series of
	 * transformations.
	 * 
	 * @param original
	 *            the original XML document
	 * @param newDataSets
	 *            the resulting data sets from the transformations
	 * @return an new XML document
	 */
	XmlObject updateXml(XmlObject original, DataSet... newDataSets);

	/**
	 * Returns the schema type specifying the type of XML document the updater
	 * can support.
	 * 
	 * @return the supported schema type
	 */
	SchemaType supports();

}
