package us.mtna.c2metadata.xml;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlObject;
import org.ddialliance.ddi_2_5.xml.xmlbeans.BaseElementType.Source;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CatgryType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CitationType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookDocument;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.DataDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.DerivationType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.DrvcmdType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.FileDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.ProdStmtType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.SimpleTextAndDateType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.SimpleTextType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.SoftwareType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.VarType;
import org.openmetadata.util.xmlbeans.XhtmlUtilities;
import org.slf4j.LoggerFactory;

import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.Metadata;
import us.mtna.pojo.Transform;
import us.mtna.pojo.TransformCommand;
import us.mtna.pojo.Variable;
import us.mtna.transform.cogs.json.TransformMapper;

public class Ddi25XmlUpdater implements XmlUpdater {

	private HashSet<String> fileIds;
	private HashMap<String, VarType> originalVarMap;
	private org.slf4j.Logger log = LoggerFactory.getLogger(Ddi25XmlUpdater.class);
	
	public Ddi25XmlUpdater() {
		fileIds = new HashSet<>();
		originalVarMap = new HashMap<>();
	}

	@Override
	public XmlObject updateXml(XmlObject original, DataSet... newDataSets) {
		CodeBookDocument originalDocument = (CodeBookDocument) original.changeType(CodeBookDocument.type);
		CodeBookDocument updatedDocument = initializeDocument();
		copyOriginalXml(originalDocument.getCodeBook(), updatedDocument.getCodeBook());
		int varCount = originalVarMap.size();
		int fileCount = fileIds.size();
		for (DataSet dataSet : newDataSets) {
			log.debug("Updating XML for dataset [" + dataSet.getId() + "]");
			String fileId;
			while (fileIds.contains((fileId = "F" + ++fileCount)))
				;
			FileDscrType fileDscr = updatedDocument.getCodeBook().addNewFileDscr();
			fileDscr.setID(fileId);
			DataDscrType dataDscr = updatedDocument.getCodeBook().addNewDataDscr();
			Metadata metadata = dataSet.getMetadata();
			int fileVarCount = 0;
			for (Variable variable : metadata.getVariables()) {
				VarType var = originalVarMap.get(variable.getId());
				LinkedHashSet<String> sources = new LinkedHashSet<>();
				boolean copied = false;
				if (var == null) {
					var = dataDscr.addNewVar();
					log.trace("no variable found in the original variable map for [" + variable.getId()
							+ "] so creating a new one");
				} else {
					// copied
					copied = true;
					sources.add(var.getID());
					log.trace("Found variable [" + variable.getId()
							+ "] in the original variable map, adding it to derivation sources");
				}

				String newId;
				while (originalVarMap.containsKey((newId = "V" + ++varCount)))
					;
				var.setID(newId);
				DerivationType derivation = var.addNewDerivation();
				if (variable.getTransforms().length > 0) {
					// update name
					var.setName(variable.getName());
					// update label
					var.getLablList().clear();
					String label = variable.getLabel();
					if (label != null && !label.isEmpty()) {
						XhtmlUtilities.setXhtmlContent(var.addNewLabl(), label);
					}
					// update categories
					String classificationId = variable.getClassificationId();
					if (classificationId != null && !classificationId.isEmpty()) {
						var.getCatgryList().clear();
						Classification classification = metadata.getClassifs().get(classificationId);
						if (classification != null) {
							for (Code code : classification.getCodeList()) {
								CatgryType catgry = var.addNewCatgry();
								XhtmlUtilities.setXhtmlContent(catgry.addNewCatValu(), code.getCodeValue());
								String codeLabel = code.getLabel();
								if (codeLabel != null && !codeLabel.isEmpty()) {
									XhtmlUtilities.setXhtmlContent(catgry.addNewLabl(), codeLabel);
								}
								if (code.isMissing()) {
									catgry.setMissing(
											org.ddialliance.ddi_2_5.xml.xmlbeans.CatgryType.Missing.Enum.forInt(1));
								}
							}
						}
					}else{
						log.debug("No classification found on variable [" + variable.getId()+"]");
					}
					// process transforms
					for (Transform transform : variable.getTransforms()) {
						sources.addAll(transform.getSourceIds());
						if (transform.getDescription() != null) {
							XhtmlUtilities.setXhtmlContent(derivation.addNewDrvdesc(), transform.getDescription());
						}
						DrvcmdType drvcmd = derivation.addNewDrvcmd();
						drvcmd.setSource(Source.PRODUCER);
						drvcmd.setSyntax(transform.getOriginalCommand().getSyntax());
						XhtmlUtilities.setXhtmlContent(drvcmd, transform.getOriginalCommand().getCommand());
						for (TransformCommand command : transform.getAlternativeCommands()) {
							if (TransformMapper.SDTL_PSEUDOCODE.equals(command.getSyntax())) {
								XhtmlUtilities.setXhtmlContent(derivation.addNewDrvdesc(), command.getCommand());
							}else {
								drvcmd = derivation.addNewDrvcmd();
								drvcmd.setSource(Source.ARCHIVE);
								drvcmd.setSyntax(command.getSyntax());								
								XhtmlUtilities.setXhtmlContent(drvcmd, command.getCommand());
							}
						}
					}
				}
				derivation.setVar(new ArrayList<>(sources));
				// add to file, if not deleted
				if (!variable.isDeleted()) {
					fileVarCount++;
					var.setFiles(Arrays.asList(fileId));
				}
				if (copied) {
					dataDscr.getVarList().add(var);
				}
			}
			// set file variable count
			XhtmlUtilities.setXhtmlContent(fileDscr.addNewFileTxt().addNewDimensns().addNewVarQnty(),
					String.valueOf(fileVarCount));
		}
		return updatedDocument;
	}

	@Override
	public SchemaType supports() {
		return CodeBookDocument.type;
	}

	private CodeBookDocument initializeDocument() {
		CodeBookDocument document = CodeBookDocument.Factory.newInstance();
		CodeBookType documentXml = document.addNewCodeBook();
		// TODO random ID?
		String id = "SAMPLE_XU_OUTPUT";
		documentXml.setID(id);
		CitationType citation = documentXml.addNewStdyDscr().addNewCitation();
		XhtmlUtilities.setXhtmlContent(citation.addNewTitlStmt().addNewTitl(), id);
		ProdStmtType prodStmt = citation.addNewProdStmt();
		SimpleTextAndDateType prodDate = prodStmt.addNewProdDate();
		LocalDate now = LocalDate.now();
		prodDate.setDate(now.toString());
		XhtmlUtilities.setXhtmlContent(prodDate, now.toString());
		SoftwareType software = prodStmt.addNewSoftware();
		// TODO get software info dynamically
		software.setVersion("1.0.0");
		software.setDate(now.toString());
		XhtmlUtilities.setXhtmlContent(software, "C2Metadata XML Updater");
		return document;
	}

	private void copyOriginalXml(CodeBookType original, CodeBookType updated) {
		// copy files
		for (FileDscrType fileDscr : original.getFileDscrList()) {
			fileIds.add(fileDscr.getID());
			updated.getFileDscrList().add((FileDscrType) fileDscr.copy());
		}
		// copy variables
		for (DataDscrType dataDscr : original.getDataDscrList()) {
			updated.getDataDscrList().add((DataDscrType) dataDscr.copy());
			for (VarType var : dataDscr.getVarList()) {
				var = (VarType) var.copy();
				var.getSumStatList().clear();
				var.getLocationList().clear();
				var.unsetFiles();
				// strip the valRange and inValRange
				int vr = var.sizeOfValrngArray();
				if (vr > 0) {
					var.removeValrng(vr - 1);
				}
				int ivr = var.sizeOfInvalrngArray();
				if (ivr > 0) {
					var.removeInvalrng(ivr - 1);
				}
				for (CatgryType cat : var.getCatgryList()) {
					cat.getCatStatList().clear();
				}
				originalVarMap.put(var.getID(), var);
			}
		}
	}

	public void setLog(org.slf4j.Logger log) {
		this.log = log;
	}

	public org.slf4j.Logger getLog() {
		return log;
	}
}
